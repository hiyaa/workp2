- ARM License Serial# G9826-05791-9E743

- FPGA Design Software
  : www.altera.com/download

- For complete software licensing and downloading instructions....  
  : https://www.altera.com/download/software/soc-eds

- Altera SoC EDS and all of its components are directly supported by Altera
  : www.altera.com/mysupport

- Boards and Device Document
  : www.altera.com/products/devkits/altera/kit-cyclone-v-soc.html
  : www.altera.com/literature/lit-cyclone-v.jsp